.. Trusted Substrate documentation master file, created by
   sphinx-quickstart on Wed May 25 16:19:39 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

###########################################
Welcome to Trusted Substrate documentation!
###########################################

.. toctree::
    :maxdepth: 2
   
    general/index
    hardware/index
    building/index
    running/index
    references
    conventions
