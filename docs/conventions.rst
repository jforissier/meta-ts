#######################
Terms and abbreviations
#######################

This document uses the following terms and abbreviations.

.. glossary::

    UEFI
        Unified Extensible Firmware Interface.

    EBBR
        Embedded Base Boot Requirements

    TPM
       Trusted Platform Module

    PK
        Platform Key

    KEK
        Key Exhange Key

    db
        Signature Database

    dbx
        Forbidden Signature Database

    ESP
        EFI System Partition

    RPMB
        Replay Protected Memory Block

    TCG
        Trusted Computing Group
